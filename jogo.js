var jogador = 0
var jogadas = 0
function clique(id) {
    console.log("jogador: "+jogador)
    var tag = null
    if(id.currentTarget)
        tag = id.currentTarget; 
    else
        tag = document.getElementById('casa'+id)

    if(tag.style.backgroundImage == '' || tag.style.backgroundImage == null)
    {
        var endereco = 'img/'+jogador+'.png'
        tag.style.backgroundImage = 'url('+endereco+')'
        jogadas += 1

        var casa = tag.id[4] 
        casa = Number(casa)
		var ganhou = verificarGanhador(casa)
        var concluSpan = document.getElementById("conclusao")

        switch(ganhou)
        {
            case 1:
                if(jogador == 0){
                    concluSpan.innerHTML= 'Resultado: Jerry é o vencedor!'
                    tag = null
                }else{
                    concluSpan.innerHTML= 'Resultado: Tom é o vencedor!'
                    tag = null
                }
                break
            case -1:
                    concluSpan.innerHTML= 'Resultado: Empate!'
                    tag = null
                break
            case 0:
                break
        }

        var botao = document.getElementById('botao')

        vez = document.getElementById('vez')
        if(jogador == 0)
        {
            jogador = 1
            vez.innerHTML = 'Tom'
        }else
        {
            jogador = 0
            vez.innerHTML = 'Jerry'
        }

        if(ganhou!=0)
        {
            finalizar()
            botao.innerHTML = '<button onclick="iniciar()" >Jogar novamente!!</button>'
        }
    }
}

function verificarGanhador(id){
    console.log(id)
        if(jogadas >=5){
        const possi = {
            '1': [[1, 2, 3], [1, 4, 7], [1, 5, 9]],
            '2': [[1, 2, 3], [2, 5, 8]],
            '3': [[1, 2, 3], [3, 6, 9], [3, 5, 7]],
            '4': [[4, 5, 6], [1, 4, 7]],
            '5': [[4, 5, 6], [2, 5, 8], [1, 5, 9], [3, 5, 7]],
            '6': [[4, 5, 6], [3, 6, 9]],
            '7': [[7, 8, 9], [1, 4, 7], [7, 5, 3]],
            '8': [[7, 8, 9], [2, 5, 8]],
            '9': [[7, 8, 9], [3, 6, 9], [1, 5, 9]]}
            casas = document.getElementsByClassName('casa')
            let check = possi[id]

            for (let i = 0; i < check.length; i++) {
                const [a, b, c] = check[i]

                if( casas[a-1].style.backgroundImage === casas[b-1].style.backgroundImage &&
                    casas[a-1].style.backgroundImage === casas[c-1].style.backgroundImage &&
                    casas[a-1].style.backgroundImage != '') {    
                    return 1

                }                                    
            }
        }
                if (jogadas == 9){
                    return -1
                }else{
                    return 0
                }
        
        

}

function finalizar(){
    tags = document.getElementsByClassName('casa')

    for(i=0; i<9; i++){
        tags[i].onclick = null
    }
}

function iniciar() {
    var concluSpan = document.getElementById("conclusao")
    
    if (concluSpan.textContent == 'Resultado: Jerry é o vencedor!')
        jogador = 1
    else
        jogador = 0
    
    jogadas = 0

    casas = document.getElementsByClassName('casa')

    for(i=0; i<9; i++){
        casas[i].onclick=clique;
        casas[i].style.backgroundImage=''
    }

    vez = document.getElementById('vez')

    botao.innerHTML = ''
    concluSpan.innerHTML = 'Resultado: '

}